<?php

/**
 * Criando uma conexão com mysqli
 * 
 * Php version PHP_VERSION
 * 
 */

$conn = new mysqli("localhost", "root", "456123", "dbphp7");

if ($conn->connect_error) {

    echo "Error: " . $conn->connect_error;

}

$result = $conn->query("SELECT * FROM tb_usuarios ORDER BY deslogin");

$data = array();

while ($row = $result->fetch_assoc()) {

    array_push($data, $row);


}

echo json_encode($data);

?>