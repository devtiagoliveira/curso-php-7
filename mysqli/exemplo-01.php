<?php

/**
 * Criando uma conexão com mysqli
 * 
 * Php version PHP_VERSION
 * 
 */

$conn = new mysqli("localhost", "root", "456123", "dbphp7");

if ($conn->connect_error) {

    echo "Error: " . $conn->connect_error;

}

$stmt = $conn->prepare("INSERT INTO tb_usuarios (deslogin, dessenha) VALUES (?, ?)");

// Os tipos de dados das ? sao definidos pelas letras, s = string, i = integer
$stmt->bind_param("ss", $login, $pass); // $stmt=->bind_param("ss", "user", "123456"); não pode passar diretamente

$login = "user";
$pass = "12345";

$stmt->execute();

?>